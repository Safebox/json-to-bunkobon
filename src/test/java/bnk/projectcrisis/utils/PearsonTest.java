package bnk.projectcrisis.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PearsonTest
{
    List<String> domains;

    @Before
    public void setUp() throws Exception
    {
        domains = new ArrayList<>();
        domains.add("Pokemon Time Jumpers");
        domains.add("Time Jumpers");
        domains.add("Doctor Omega");
        domains.add("Ω");
    }

    @Test
    public void generateHashFromString()
    {
        for (String domain : domains)
        {
            Pearson.generateHashFromString(domain);
        }
    }
}