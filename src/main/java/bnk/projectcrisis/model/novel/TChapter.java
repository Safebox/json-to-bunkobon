package bnk.projectcrisis.model.novel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonPropertyOrder(value = {"head", "body"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = TChapter.TChapterBuilder.class)
public class TChapter
{
    private String title;
    private String domain;
    private String location;
    private String daysToCrisis;
    private String content;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TChapterBuilder
    {

    }
}
