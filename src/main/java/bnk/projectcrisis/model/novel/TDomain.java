package bnk.projectcrisis.model.novel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.util.LinkedHashMap;

@Data
@Builder
@JsonPropertyOrder(value = {"locations"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = TDomain.TDomainBuilder.class)
public class TDomain
{
    private LinkedHashMap<String, String> locations;

    public static TDomainBuilder emptyBuilder()
    {
        return TDomain.builder()
                .locations(new LinkedHashMap<String, String>());
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class TDomainBuilder
    {

    }
}
