package bnk.projectcrisis.model.script;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonPropertyOrder(value = {"firstName", "middleName", "lastName", "domain"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = TCharacter.TCharacterBuilder.class)
public class TCharacter
{
    private String firstName;
    private String middleName;
    private String lastName;
    private String domain;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TCharacterBuilder
    {

    }

    public String getProperty(String id)
    {
        switch (id.toLowerCase())
        {
            case "fn":
            case "firstname":
                return firstName;
            case "mn":
            case "middlename":
                return middleName;
            case "ln":
            case "lastname":
                return lastName;
            default:
                return domain;
        }
    }
}
