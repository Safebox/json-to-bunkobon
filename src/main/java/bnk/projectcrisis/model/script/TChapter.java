package bnk.projectcrisis.model.script;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonPropertyOrder(value = {"head", "body"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = TChapter.TChapterBuilder.class)
public class TChapter
{
    private Boolean newPage;
    private String title;
    private String domain;
    private String location;
    private String character;
    private String parenthesis;
    private String dialogue;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TChapterBuilder
    {

    }
}
