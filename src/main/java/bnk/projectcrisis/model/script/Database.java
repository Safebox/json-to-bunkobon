package bnk.projectcrisis.model.script;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.LinkedList;

@Data
@Builder
@JsonPropertyOrder(value = {"tCharacters", "tDomains", "tChapters"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = Database.DatabaseBuilder.class)
public class Database
{
    @JsonProperty("tCharacters")
    private LinkedHashMap<String, TCharacter> tCharacters;
    @JsonProperty("tDomains")
    private LinkedHashMap<String, TDomain> tDomains;
    @JsonProperty("tChapters")
    private LinkedList<String> tChapters;
    @JsonProperty("tNotes")
    private LinkedList<String> tNotes;

    public static DatabaseBuilder emptyBuilder()
    {
        return Database.builder()
                .tCharacters(new LinkedHashMap<String, TCharacter>())
                .tDomains(new LinkedHashMap<String, TDomain>())
                .tChapters(new LinkedList<String>())
                .tNotes(new LinkedList<String>());
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class DatabaseBuilder
    {

    }
}
