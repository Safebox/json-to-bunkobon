package bnk.projectcrisis;

import bnk.projectcrisis.model.novel.Database;
import bnk.projectcrisis.model.novel.TCharacter;
import bnk.projectcrisis.model.novel.TDomain;
import bnk.projectcrisis.model.novel.TChapter;
import bnk.projectcrisis.utils.Pearson;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.File;
import java.io.IOException;

public class Init
{
    public static void main(String[] args) throws IOException
    {
        ObjectWriter objectMapper = new ObjectMapper().writer().withDefaultPrettyPrinter();
        initNovel(objectMapper);
        initScript(objectMapper);
    }

    private static void initNovel(ObjectWriter objectMapper) throws IOException
    {
        TCharacter tCharacter = TCharacter.builder()
                .firstName("")
                .middleName("")
                .lastName("")
                .domain(Pearson.generateHashFromString(""))
                .build();
        Database database = Database.emptyBuilder().build();
        database.getTCharacters().put("1", tCharacter);
        database.getTCharacters().put("2", tCharacter);
        database.getTDomains().put("1", TDomain.emptyBuilder().build());
        database.getTDomains().get("1").getLocations().put("A", "");
        database.getTDomains().get("1").getLocations().put("B", "");
        database.getTChapters().put("1", TChapter.builder().title("Title").domain("0").location("0").daysToCrisis("365").content("Hello world!").build());
        objectMapper.writeValue(new File("novelDatabase.json"), database);
    }

    private static void initScript(ObjectWriter objectMapper) throws IOException
    {
        bnk.projectcrisis.model.script.TCharacter tCharacter = bnk.projectcrisis.model.script.TCharacter.builder()
                .firstName("")
                .middleName("")
                .lastName("")
                .domain(Pearson.generateHashFromString(""))
                .build();
        bnk.projectcrisis.model.script.Database database = bnk.projectcrisis.model.script.Database.emptyBuilder().build();
        database.getTCharacters().put("1", tCharacter);
        database.getTCharacters().put("2", tCharacter);
        database.getTDomains().put("1", bnk.projectcrisis.model.script.TDomain.emptyBuilder().build());
        database.getTDomains().get("1").getLocations().put("A", "");
        database.getTDomains().get("1").getLocations().put("B", "");
        database.getTChapters().add("element:content");
        objectMapper.writeValue(new File("scriptDatabase.json"), database);
    }
}
