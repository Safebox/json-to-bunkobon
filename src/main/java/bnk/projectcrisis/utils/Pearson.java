package bnk.projectcrisis.utils;

public class Pearson
{
    public static String generateHashFromString(String s)
    {
        int hash = 0;
        for (char c : s.toCharArray())
        {
            hash = hash ^ c;
        }
        System.out.println("\"" + s + "\" = " + hash);
        return String.valueOf(hash);
    }
}
