package bnk.projectcrisis;

import bnk.projectcrisis.model.novel.Database;
import bnk.projectcrisis.model.novel.TChapter;
import bnk.projectcrisis.model.novel.TCharacter;
import bnk.projectcrisis.model.novel.TDomain;
import com.fasterxml.jackson.databind.ObjectMapper;
import htmlflow.HtmlView;
import htmlflow.StaticHtml;
import org.xmlet.htmlapifaster.Body;
import org.xmlet.htmlapifaster.Div;
import org.xmlet.htmlapifaster.EnumRelType;
import org.xmlet.htmlapifaster.Html;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Build
{
    public static void main(String[] args) throws IOException, URISyntaxException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        buildNovel(objectMapper);
        buildScript(objectMapper);
        buildData(objectMapper);
    }

    private static void buildNovel(ObjectMapper objectMapper) throws IOException
    {
        Database database = objectMapper.readValue(Build.class.getResource("/novelDatabaseTrue.json"), Database.class);
        Body<Html<HtmlView>> start = StaticHtml.view()
                .html()
                .head()
                .link()
                .attrHref("https://fonts.googleapis.com/css?family=Lexend+Deca&display=swap").attrRel(EnumRelType.STYLESHEET)
                .__()
                .style()
                .text("body {\n" +
                        "  background: rgb(204,204,204);\n" +
                        "  font-family: 'Lexend Deca', sans-serif;\n" +
                        "}\n" +
                        "p {\n" +
                        "  font-size: 10pt;\n" +
                        //"  text-indent: 1em;\n" +
                        "}\n" +
                        ".a6 {\n" +
                        "  background: white;\n" +
                        "  display: block;\n" +
                        "  margin: 0 auto;\n" +
                        "  margin-bottom: 5mm;\n" +
                        "  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);\n" +
                        "  padding: 5mm;\n" +
                        "  width: calc(105mm - 5mm);\n" +
                        //"  height: calc(148mm - 5mm);\n" +
                        "  text-align: justify;\n" +
                        "}\n" +
                        "h3, h4, h5 {\n" +
                        "  margin: 0;\n" +
                        "}")
                .__()
                .__()
                .body();

        HtmlView htmlView = StaticHtml.view();
        start = start.of(body ->
        {
            Div<Body<Html<HtmlView>>> div = StaticHtml.view().html().body().div();
            int ch = 0;
            for (TChapter chapter : database.getTChapters().values())
            {
                if (chapter.getTitle() != null && !chapter.getTitle().isEmpty())
                {
                    if (ch > 0)
                    {
                        div.__();
                    }
                    div = body.div().attrClass("a6")
                            .h2().addAttr("align", "center").text(chapter.getTitle())
                            .__()
                            .of(d ->
                            {
                                writeChapters(d, chapter, database.getTDomains().get(chapter.getDomain()), database.getTCharacters());
                            });
                }
                else
                {
                    div = writeChapters(div, chapter, database.getTDomains().get(chapter.getDomain()), database.getTCharacters());
                }
                ch++;
            }
            Div<Body<Html<HtmlView>>> finalDiv = div;
            body.of(b -> finalDiv.__());
        });
        htmlView.addPartial(start.__().__());

        FileWriter fileWriter = new FileWriter(new File("novelPage.html"));
        fileWriter.write(htmlView.render().replaceAll("<p>", "<p>　"));
        fileWriter.close();
    }

    private static Div writeChapters(Div<Body<Html<HtmlView>>> div, TChapter chapter, TDomain domain, HashMap<String, TCharacter> characters)
    {
        boolean domainExists = (chapter.getDomain() != null && !chapter.getDomain().isEmpty());
        boolean locationExists = (chapter.getLocation() != null && !chapter.getLocation().isEmpty());
        boolean countdownExists = (chapter.getDaysToCrisis() != null && !chapter.getDaysToCrisis().isEmpty());
        if (domainExists)
        {
            div.h3().addAttr("align", "center")
                    .text("Domain-" + chapter.getDomain())
                    .__();
        }
        if (locationExists)
        {
            div.h4().addAttr("align", "center")
                    .text(domain.getLocations().get(chapter.getLocation()))
                    .__();
        }
        if (countdownExists)
        {
            div.h5().addAttr("align", "center")
                    .text(chapter.getDaysToCrisis() != null && !chapter.getDaysToCrisis().isEmpty() ? chapter.getDaysToCrisis() + " days till crisis" : "")
                    .__();
        }
        div.p().of(p ->
        {
            String content = chapter.getContent();
            Pattern pattern = Pattern.compile("\\{(.*?):([0-3])\\}");
            Matcher matcher = pattern.matcher(content);

            while (matcher.find())
            {
                content = content.replace(matcher.group(0), (characters.containsKey(matcher.group(1)) ? characters.get(matcher.group(1)).getProperty(Integer.valueOf(matcher.group(2))) : "NUL"));
            }
            p.text(content.replaceAll("\n\n", "<br><br>　")
                    .replaceAll("\n\"", "<br>　\"")
                    .replaceAll("\n", "<br>"));
        })
                .__();
        return div;
    }

    private static void buildScript(ObjectMapper objectMapper) throws IOException
    {
        bnk.projectcrisis.model.script.Database database = objectMapper.readValue(Build.class.getResource("/scriptDatabaseTrue.json"), bnk.projectcrisis.model.script.Database.class);
        Body<Html<HtmlView>> start = StaticHtml.view()
                .html()
                .head()
                .link()
                .attrHref("https://fonts.googleapis.com/css?family=Lexend+Deca&display=swap").attrRel(EnumRelType.STYLESHEET)
                .__()
                .style()
                .text("body {\n" +
                        "  background: rgb(204,204,204);\n" +
                        "  font-family: 'Lexend Deca', sans-serif;\n" +
                        "}\n" +
                        "p {\n" +
                        "  font-size: 10pt;\n" +
                        //"  text-indent: 1em;\n" +
                        "}\n" +
                        ".a6 {\n" +
                        "  background: white;\n" +
                        "  display: block;\n" +
                        "  margin: 0 auto;\n" +
                        "  margin-bottom: 5mm;\n" +
                        "  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);\n" +
                        "  padding: 5mm;\n" +
                        "  width: calc(105mm - 5mm);\n" +
                        //"  height: calc(148mm - 5mm);\n" +
                        "  text-align: justify;\n" +
                        "}\n" +
                        "h3, h4, h5 {\n" +
                        "  margin: 0;\n" +
                        "}")
                .__()
                .__()
                .body();

        HtmlView htmlView = StaticHtml.view();
        start = start.of(body ->
        {
            Div<Body<Html<HtmlView>>> div = body.div().attrClass("a6");
            for (String chapter : database.getTChapters())
            {
                Pattern patternSplitter = Pattern.compile("(.*?):(.*)");
                Matcher matcherSplitter = patternSplitter.matcher(chapter);

                if (matcherSplitter.find())
                {
                    switch (matcherSplitter.group(1))
                    {
                        case "#":
                        case "ttl":
                        case "title":
                            div = div.h3().addAttr("align", "center")
                                    .text(matcherSplitter.group(2))
                                    .__();
                            break;
                        case "[]":
                        case "scn":
                        case "scene":
                            div = div.h4().addAttr("align", "center").attrStyle("margin-top: 0;")
                                    .text("[" + matcherSplitter.group(2) + "]")
                                    .__();
                            break;
                        case "act":
                        case "action":
                            div = div.p()
                                    .text(matcherSplitter.group(2))
                                    .__();
                            break;
                        case "^":
                        case "chr":
                        case "char":
                        case "character":
                            div = div.p().attrStyle("margin-bottom: 0;")
                                    .b()
                                    .text(matcherSplitter.group(2))
                                    .__()
                                    .__();
                            break;
                        case "dia":
                        case "dial":
                        case "dialogue":
                            div = div.p().attrStyle("margin-top: 0; padding-left: 5mm;")
                                    .text(matcherSplitter.group(2))
                                    .__();
                            break;
                        case "()":
                        case "par":
                        case "para":
                        case "parenthetical":
                            div = div.p().attrStyle("margin-top: 0; margin-bottom: 0;")
                                    .i()
                                    .text("(" + matcherSplitter.group(2) + ")")
                                    .__()
                                    .__();
                            break;
                        case "lyr":
                        case "lyric":
                            div = div.p()
                                    .i()
                                    .text("♫" + matcherSplitter.group(2))
                                    .__()
                                    .__();
                            break;
                        case ">":
                        case "gt":
                        case "goto":
                            div = div.h4().addAttr("align", "right").attrStyle("margin-bottom: 0;")
                                    .text(matcherSplitter.group(2))
                                    .__();
                            break;
                        case "===":
                            div = div.__().div().attrClass("a6");
                            break;
                        default:
                            div = div.br().__();
                            break;
                    }
                }
                else
                {
                    div = div.br().__();
                }
            }
            Div<Body<Html<HtmlView>>> finalDiv = div;
            body.of(b -> finalDiv.__());
        });
        htmlView.addPartial(start.__().__());

        String outputString = htmlView.render();
        Pattern patternVariable = Pattern.compile("\\{(.*?):(.*?):(.*?)\\}");
        Matcher matcherVariable = patternVariable.matcher(outputString);
        while (matcherVariable.find())
        {
            switch (matcherVariable.group(1))
            {
                case "[]":
                case "scn":
                case "scene":
                    if (database.getTDomains().containsKey(matcherVariable.group(2)))
                    {
                        outputString = outputString.replace(matcherVariable.group(0), database.getTDomains().get(matcherVariable.group(2)).getProperty(matcherVariable.group(3)));
                    }
                    break;
                case "^":
                case "chr":
                case "char":
                case "character":
                    if (database.getTCharacters().containsKey(matcherVariable.group(2)))
                    {
                        outputString = outputString.replace(matcherVariable.group(0), database.getTCharacters().get(matcherVariable.group(2)).getProperty(matcherVariable.group(3)));
                    }
                    break;
            }
        }

        FileWriter fileWriter = new FileWriter(new File("scriptPage.html"));
        fileWriter.write(outputString);
        fileWriter.close();
    }

    private static void buildData(ObjectMapper objectMapper) throws IOException, URISyntaxException
    {
        bnk.projectcrisis.model.script.Database database = objectMapper.readValue(Build.class.getResource("/scriptDatabaseTrue.json"), bnk.projectcrisis.model.script.Database.class);
        Scanner script = new Scanner(new File(Build.class.getResource("/scriptDatabaseTrue.dat").toURI()));
        Body<Html<HtmlView>> start = StaticHtml.view()
                .html()
                .head()
                .link()
                .attrHref("https://fonts.googleapis.com/css?family=Lexend+Deca&display=swap").attrRel(EnumRelType.STYLESHEET)
                .__()
                .style()
                .text("body {\n" +
                        "  background: rgb(204,204,204);\n" +
                        "  font-family: 'Lexend Deca', sans-serif;\n" +
                        "}\n" +
                        "p {\n" +
                        "  font-size: 10pt;\n" +
                        //"  text-indent: 1em;\n" +
                        "}\n" +
                        ".a6 {\n" +
                        "  background: white;\n" +
                        "  display: block;\n" +
                        "  margin: 0 auto;\n" +
                        "  margin-bottom: 5mm;\n" +
                        "  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);\n" +
                        "  padding: 5mm;\n" +
                        "  width: calc(105mm - 5mm);\n" +
                        //"  height: calc(148mm - 5mm);\n" +
                        "  text-align: justify;\n" +
                        "}\n" +
                        "h3, h4, h5 {\n" +
                        "  margin: 0;\n" +
                        "}")
                .__()
                .__()
                .body();

        HtmlView htmlView = StaticHtml.view();
        start = start.of(body ->
        {
            Div<Body<Html<HtmlView>>> div = body.div().attrClass("a6");
            while (script.hasNextLine())
            {
                String chapter = script.nextLine();
                Pattern patternSplitter = Pattern.compile("^(===)$|^([#^>~])(.*)$|^/(.*)/$|^\\[(.*)\\]$|^(.*):$|^(.*)$");
                Matcher matcherSplitter = patternSplitter.matcher(chapter);

                if (matcherSplitter.find())
                {
                    if (matcherSplitter.group(1) != null)
                    {
                        div = div.__().div().attrClass("a6");
                    }
                    else if (matcherSplitter.group(2) != null)
                    {
                        switch (matcherSplitter.group(2))
                        {
                            case "#":
                                div = div.h3().addAttr("align", "center")
                                        .text(matcherSplitter.group(3).trim())
                                        .__();
                                break;
                            case "^":
                                div = div.p().attrStyle("margin-bottom: 0;")
                                        .b()
                                        .text(matcherSplitter.group(3).trim())
                                        .__()
                                        .__();
                                break;
                            case ">":
                                div = div.p().attrStyle("margin-top: 0; padding-left: 5mm;")
                                        .text(matcherSplitter.group(3).trim())
                                        .__();
                                break;
                            case "~":
                                div = div.p()
                                        .i()
                                        .text("♫" + matcherSplitter.group(3).trim())
                                        .__()
                                        .__();
                                break;
                        }
                    }
                    else if (matcherSplitter.group(4) != null)
                    {
                        div = div.p().attrStyle("margin-top: 0; margin-bottom: 0;")
                                .i()
                                .text("(" + matcherSplitter.group(4) + ")")
                                .__()
                                .__();
                    }
                    else if (matcherSplitter.group(5) != null)
                    {
                        div = div.h4().addAttr("align", "center").attrStyle("margin-top: 0;")
                                .text("[" + matcherSplitter.group(5) + "]")
                                .__();
                    }
                    else if (matcherSplitter.group(6) != null)
                    {
                        div = div.h4().addAttr("align", "right").attrStyle("margin-bottom: 0;")
                                .text(matcherSplitter.group(6) + ":")
                                .__();
                    }
                    else if (matcherSplitter.group(7) != null)
                    {
                        div = div.p()
                                .text(matcherSplitter.group(7))
                                .__();
                    }
                    else
                    {
                        div = div.br().__();
                    }
                }
                else
                {
                    div = div.br().__();
                }
            }
            Div<Body<Html<HtmlView>>> finalDiv = div;
            body.of(b -> finalDiv.__());
        });
        htmlView.addPartial(start.__().__());

        String outputString = htmlView.render();
        Pattern patternVariable = Pattern.compile("\\{(.*?):(.*?):(.*?)\\}");
        Matcher matcherVariable = patternVariable.matcher(outputString);
        while (matcherVariable.find())
        {
            switch (matcherVariable.group(1))
            {
                case "[]":
                    if (database.getTDomains().containsKey(matcherVariable.group(2)))
                    {
                        outputString = outputString.replace(matcherVariable.group(0), database.getTDomains().get(matcherVariable.group(2)).getProperty(matcherVariable.group(3)));
                    }
                    break;
                case "^":
                    if (database.getTCharacters().containsKey(matcherVariable.group(2)))
                    {
                        outputString = outputString.replace(matcherVariable.group(0), database.getTCharacters().get(matcherVariable.group(2)).getProperty(matcherVariable.group(3)));
                    }
                    break;
            }
        }

        FileWriter fileWriter = new FileWriter(new File("dataPage.html"));
        fileWriter.write(outputString);
        fileWriter.close();
    }
}
