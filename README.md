# Script
## Types
### Title
- `#`
- `ttl`
- `title`

```
<h3 align="center">
    Title
</h3>
```

### Scene
- `[]`
- `scn`
- `scene`

```
<h4 align="center" style="margin-top: 0;">
    [Scene]
</h4>
```

### Action
- `act`
- `action`

```
<p>
    Action
</p>
```

### Character
- `^`
- `chr`
- `char`
- `character`

```
<p style="margin-bottom: 0;">
    <b>
        Character
    </b>
</p>
```

### Dialogue
- `dia`
- `dial`
- `dialogue`

```
<p style="margin-top: 0; padding-left: 5mm;">
    Dialogue
</p>
```

### Emotive (Parenthetical)
- `()`
- `par`
- `para`
- `parenthetical`

```
<p style="margin-top: 0; margin-bottom: 0;">
    <i>
        (Parenthetical)
    </i>
</p>
```

### Lyric
- `lyr`
- `lyric`

```
<p>
    <i>
        ♫Lyric
    </i>
</p>
```

### Transition
- `>`
- `gt`
- `goto`

```
<h4 align="right" style="margin-bottom: 0;">
    Transition:
</h4>
```

### Page Break
- `===`

## Tags
### Scene
- `{[]:key:property}`
- `{scn:key:property}`
- `{scene:key:property}`

### Character
- `{^:key:property}`
- `{chr:key:property}`
- `{char:key:property}`
- `{character:key:property}`

# Markup
## Types
### Title
- `# Title`

```
<h3 align="center">
    Title
</h3>
```

### Scene
- `[Scene]`

```
<h4 align="center" style="margin-top: 0;">
    [Scene]
</h4>
```

### Action
- `Action`

```
<p>
    Action
</p>
```

### Character
- `^ Character`

```
<p style="margin-bottom: 0;">
    <b>
        Character
    </b>
</p>
```

### Dialogue
- `> Dialogue`

```
<p style="margin-top: 0; padding-left: 5mm;">
    Dialogue
</p>
```

### Emotive (Parenthetical)
- `/Parenthetical/`

```
<p style="margin-top: 0; margin-bottom: 0;">
    <i>
        (Parenthetical)
    </i>
</p>
```

### Lyric
- `~ Lyric`

```
<p>
    <i>
        ♫Lyric
    </i>
</p>
```

### Transition
- `Transition:`

```
<h4 align="right" style="margin-bottom: 0;">
    Transition:
</h4>
```

### Page Break
- `===`

## Tags
### Scene
- `{[]:key:property}`
- `{scn:key:property}`
- `{scene:key:property}`

### Character
- `{^:key:property}`
- `{chr:key:property}`
- `{char:key:property}`
- `{character:key:property}`